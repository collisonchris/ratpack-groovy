# ratpack-groovy

Ratpack is a lightweight set of libraries designed to build minimal web based applications.

To run this sample, just clone the repo and run `./gradlew run` This will start a web server on port 5050 so you can hit it on localhost:5050 & localhost:5050/simple