package com.aimia.service

import spock.lang.Specification

class SimpleServiceSpec extends  Specification {

    SimpleService simpleService = new SimpleService()

    def "test simple service"() {
        when:
        Map output = simpleService.customRequest(param)

        then:
        output == expected


        where:
        param              | expected
        null               | ["lunch": "noodles"]
        ""                 | ["lunch": "noodles"]
        "thing"            | ["lunch": "noodles"]
        "taco"             | ["lunch": "taco"]
        "blahadsflhastaco" | ["lunch": "taco"]

    }
}
