package com.aimia.integration

import com.aimia.handler.SimpleHandler
import com.aimia.module.SimpleModule
import com.aimia.service.SimpleService
import ratpack.groovy.test.embed.GroovyEmbeddedApp
import ratpack.test.http.TestHttpClient
import spock.lang.AutoCleanup
import spock.lang.Specification

class SimpleIntegrationSpec extends Specification {

    def 'can create simple hello world'() {
        expect:
        GroovyEmbeddedApp.fromHandler {
            render 'Hello GR8ConfUS 2016!'
        } test {
            assert getText() == 'Hello GR8ConfUS 2016!'
        }
    }

    @AutoCleanup
    @Delegate
    GroovyEmbeddedApp app = GroovyEmbeddedApp.ratpack {
        bindings {
            module(SimpleModule)
            bind(SimpleHandler)
        }
        handlers {
                get("simple", SimpleHandler)
            get {
                render "hello"
            }
        }
    }

    def 'can create simple handler test - taco param'() {
        expect:
        app

        and:
        TestHttpClient client = app.httpClient
        assert client.get("/simple?lunch=taco").body.text == """{"lunch":"taco"}"""
    }

    def 'can create simple handler test - no taco param'() {
        expect:
        app

        and:
        TestHttpClient client = app.httpClient
        assert client.get("/simple?lunch=otherstuff").body.text == """{"lunch":"noodles"}"""
    }
}
