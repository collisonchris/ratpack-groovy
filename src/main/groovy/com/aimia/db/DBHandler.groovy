package com.aimia.db

import groovy.sql.GroovyRowResult
import groovy.sql.Sql

import javax.inject.Inject
import javax.inject.Singleton
import java.sql.SQLException

@Singleton
class DBHandler {

    @Inject
    Sql sql

    List<GroovyRowResult> getAccountMergeEntityGroups() throws SQLException {
        return sql.rows("SELECT * FROM ods.account_merge_entity_group")
    }

}
