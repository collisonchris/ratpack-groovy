package com.aimia.module

import com.aimia.handler.SimpleHandler
import com.aimia.service.SimpleService
import com.google.inject.AbstractModule

class SimpleModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(SimpleService)
    }
}
