package com.aimia.handler

import com.aimia.db.DBHandler
import groovy.sql.GroovyRowResult
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import ratpack.handling.Context
import ratpack.handling.Handler

import javax.inject.Inject
import javax.inject.Singleton

import static ratpack.jackson.Jackson.json

@Singleton
class SampleDBHandler implements Handler {

    private static Logger logger = LoggerFactory.getLogger(SampleDBHandler)

    @Inject DBHandler dbHandler

    @Override
    void handle(Context ctx) throws Exception {
        List<GroovyRowResult> results = []
        try {
            results = dbHandler.getAccountMergeEntityGroups()
        } catch(Exception e) {
            logger.error("Database error occurred!")
            ctx.error(e)
            return
        }
        logger.info("Request for DB info made!")
        ctx.render(json(results))
    }
}
