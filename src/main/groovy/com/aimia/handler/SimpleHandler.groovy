package com.aimia.handler

import com.aimia.service.SimpleService
import ratpack.handling.Context
import ratpack.handling.Handler

import javax.inject.Inject

import static ratpack.jackson.Jackson.json

class SimpleHandler implements Handler {

    @Inject
    SimpleService simpleService

    @Override
    void handle(Context ctx) throws Exception {
        String query = ctx.request.queryParams?.lunch
        ctx.render(json(simpleService.customRequest(query)))
    }
}