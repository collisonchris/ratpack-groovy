package com.aimia.service

import groovy.sql.GroovyRowResult
import groovy.sql.Sql

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SimpleService {

    Map<String, Object> customRequest(String param) {
        if (param?.contains("taco")) {
            return ["lunch": "taco"]
        }
        return ["lunch": "noodles"]
    }
}
