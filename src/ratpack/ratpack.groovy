package ratpack

import com.aimia.handler.SampleDBHandler
import com.aimia.handler.SimpleHandler
import com.aimia.module.SimpleModule
import com.zaxxer.hikari.HikariConfig
import ratpack.groovy.sql.SqlModule
import ratpack.hikari.HikariModule

import static ratpack.groovy.Groovy.ratpack

ratpack {
    bindings {
        //Simple bindings can be done directly here in the startup script DSL
        bind(SimpleHandler)
        bind(SampleDBHandler)
        //More complicated operations can be externalized by adding a guice module
        module(SimpleModule)
        //adding sql support via ratpack.sql & Hikari
        module SqlModule
        module HikariModule, { HikariConfig cfg ->
            cfg.setJdbcUrl("jdbc:mysql://localhost:3306/ods")
//            cfg.setDriverClassName("com.mysql.jdbc.Driver")
            cfg.setUsername("")//TODO Set me if you want to run locally
            cfg.setPassword("")//TODO Set me if you want to run locally
        }
    }
    handlers {
        //Ratpack uses a simple DSL of path->handler
        // so here hitting /simple will call the code behind the SimpleHandler class
        get("simple", SimpleHandler)
        //Simple DB access example
        get("db", SampleDBHandler)
        get {
            render "Hello world!"
        }


    }
}

//class SimpleHandler implements Handler {
//
//    @Override
//    void handle(Context ctx) throws Exception {
//        ctx.render("Hello")
//    }
//}